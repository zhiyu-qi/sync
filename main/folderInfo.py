# A2 for COMPSCI340/SOFTENG370 2015
# by Zhiyu QI, UPI: zqi675

'''
Created on 25/09/2015

@author: roko
'''
import hashlib
import json
import os
import time
from distutils.dep_util import newer

SYNC_FILE_NAME = ".sync"

class Change:
    
    def __init__(self, timeStamp, hash):
        '''
        Constructor
        '''
        self.timeStamp = timeStamp
        self.hash = hash
    
class FileInfo:
    
    def __init__(self, folderInfo):
        '''
        Constructor
        '''
        self.name = ''
        self.changes = []
        #self.folder = folderInfo
    def dump(self, fname):
        fp = open(fname,'w')
        json.dump(self, fp, cls=ComplexEncoder, indent=2)
        
class ComplexEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, FileInfo):
            return {obj.name:obj.changes}
        if isinstance(obj, Change):
            return [obj.timeStamp, obj.hash]
        # Let the base class default method raise the TypeError
        return json.JSONEncoder.default(self, obj)
                
class FolderInfo(object):
    def hash(self, fname):
        _hash = hashlib.sha256()
        with open(fname, encoding='utf-8') as f:
            for chunk in iter(lambda: f.read(4096), ""):
                _hash.update(chunk.encode('utf-8'))
        return _hash.hexdigest()
    
    def syncFile(self):
        return self.fitName(SYNC_FILE_NAME);
    
    def modTime(self, path):
        return time.strftime("%Y-%m-%d %H:%M:%S %z", time.localtime(os.path.getmtime(path)))
    
    def fitName(self, name):
        return self.path + "/" + name;
    
    def save(self):
        with open(self.syncFile(),'w', encoding='utf-8') as fp:
            json.dump(self.syncdb, fp, indent=2)

    '''
    represent a .sync for given path(full)
    '''
    def __init__(self, path):
        '''
        Constructor
        '''
        self.files = []
        self.dirs = []
        self.path = path
        #print(path)
        
        os.makedirs(path, exist_ok=True)
        for f in os.listdir(path):
            if(f==SYNC_FILE_NAME):
                continue
            
            if os.path.isdir(path+'/'+f):
                self.dirs.append(f)
            else:
                self.files.append(f)
        #print(self.files)
        self.files.sort(key=None, reverse=False)
        self.dirs.sort(key=None, reverse=False)
        
        #load json
        if not os.path.isfile(self.syncFile()):
            with open(self.syncFile(), "w" ,encoding='utf-8') as file:
                file.write("{}")
        
        fp = open(self.syncFile(),'r')
        self.syncdb = json.load(fp)
        
        #update syncdb
        # 1 - DELETED
        for f in self.syncdb:
            if f not in self.files:
                last = self.syncdb[f][0]
                self.syncdb[f].insert(0, [last[0],'DELETED'])

        # 2 - EXISTING FILES
        for f in self.files:
            newer = [self.modTime(self.fitName(f)), self.hash(self.fitName(f))]
            if f not in self.syncdb:
                #ADD 
                self.syncdb[f]=[newer]
            else:
                #UPDATE
                last = self.syncdb[f][0]
                if(newer[1]!=last[1]):
                    self.syncdb[f].insert(0, newer)
                elif(newer[0]>last[0]):
                    self.syncdb[f][0] = newer
        
        self.save()
        #print(json.dumps(self.syncdb))            
