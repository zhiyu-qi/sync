'''
Created on 24/09/2015

@author: roko
'''
import unittest
import sync
import folderInfo

class SyncTest(unittest.TestCase):
    def testHash(self):
        _sync = sync.Sync('')
        
        pass

    def testJson(self):
        fileinfo = folderInfo.FileInfo('')
        fileinfo.name = "A"
        fileinfo.changes.append([
 "2015-08-26 12:03:42 +1200",
 "c62b8de531b861db068eac1129c2e3105ab337b225339420d2627c123c7eae04"])
        fileinfo.changes.append([
 "2015-08-26 12:03:43 +1200",
 "3032e7474e22dd6f35c618045299165b0b42a9852576b7df52c1b22e3255b112"])
        fileinfo.dump("sync.json")
        pass

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()