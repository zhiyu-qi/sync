#!/usr/bin/env python3

# A2 for COMPSCI340/SOFTENG370 2015
# by Zhiyu QI, UPI: zqi675

'''
Created on 24/09/2015

@author: roko
'''
import os
import shutil
import folderInfo

USAGE = "Please check your input, and run: ./sync dir1 dir2"
class Sync(object):
    '''
    1. list files 
    2. sync sub-dirs <- make equal
    3. load 2 JSON .sync
    4. compare file size/date ... hash
    
    write .sync
    '''
    def __init__(self, argv):
        '''
        Constructor
        '''
        if len(argv) != 3:
            self.dir1=''
            self.dir2=''
            #print(argv)
            return
        
        self.dir1 = argv[1]
        self.dir2 = argv[2]
        #self.folderInfo = folderInfo.FolderInfo()
        
    def Valid(self):
        if  self.dir1=='' or self.dir2=='' or os.path.isfile(self.dir1) or os.path.isfile(self.dir2):
            print(USAGE)
            return 0
        
        if os.path.isdir(self.dir1) or os.path.isdir(self.dir2):
            return 1
        else:
            print(USAGE)
            return 0
    
    def MergeJson(self):
        return

    '''
    CHECK FILES, while .sync represents each folder
    '''
    def SyncFile(self, fi1, fi2):
        filecount = 0
        db1 = fi1.syncdb
        db2 = fi2.syncdb
        for f in db1:
            l = db1[f];
            if f in db2:
                r = db2[f];
                
                if(l[0][1] != r[0][1]):
                    direction=0
                    if r[0][1] == 'DELETED':
                        direction=1
                    elif l[0][1] == 'DELETED' or l[0][0]<r[0][0]:
                        direction=-1
                    else:
                        direction=1
                    
                    if(direction==1):
                        shutil.copyfile(fi1.fitName(f), fi2.fitName(f))
                        r.insert(0, l[0])
                    else:
                        shutil.copyfile(fi2.fitName(f), fi1.fitName(f))
                        l.insert(0, r[0])
                        
                    filecount=filecount+1
                else:
                    if(l[0][0]<r[0][0]):
                        r[0][0] = l[0][0]
                    else:
                        l[0][0] = r[0][0]
            elif l[0][1]!= 'DELETED':
                shutil.copyfile(fi1.fitName(f), fi2.fitName(f))
                db2[f]=[l[0]]
                filecount=filecount+1

        for f in db2:
            if f not in db1:
                r = db2[f];
                if r[0][1]== 'DELETED':
                    continue;
                
                shutil.copyfile(fi2.fitName(f), fi1.fitName(f))
                db1[f]=[r[0]]
                filecount=filecount+1
        
        fi1.save()
        fi2.save()
        return filecount
    
    '''
    create sub-dirs
    '''
    def SyncFolder(self, fi1, fi2):
        
        d1 = fi1.dirs
        d2 = fi2.dirs

        for d in d1:
            if d not in d2:
                os.mkdir(self.dir2+"/"+d)
                d2.append(d)
        
        for d in d2:
            if d not in d1:
                os.mkdir(self.dir1+"/"+d)
                d1.append(d)

        filecount = 0
        for d in d1:
            sync0 = Sync(['', fi1.path+"/"+d, fi2.path+"/"+d ])
            filecount+=sync0.Exec()
        return filecount
    
    def Exec(self):
        if(not self.Valid()):
            return -1
        
        fi1 = folderInfo.FolderInfo(self.dir1)
        fi2 = folderInfo.FolderInfo(self.dir2)
        #process files in current directory
        files = self.SyncFile(fi1,fi2)
        
        #build tree and recursively call Sync
        files += self.SyncFolder(fi1,fi2)
        return files


if __name__ == "__main__":
#    sys.argv = ['', 'Test.testName']
    import sys;
    _sync = Sync(sys.argv)
    #print(sys.argv)
    count = _sync.Exec()
    if(count>=0):
        print("files changed ", count)    